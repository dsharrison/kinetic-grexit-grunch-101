'use strict';

let Polygon = class {
    constructor(height, width) {
        this.height = height;
        this.width = width;
        console.dir('Constructed');
    }

    get area() {
        console.dir('Getting Area');
        return this.calcArea();
    }

    calcArea() {
        console.dir('Calculating Area');
        return this.height * this.width;
    }
};

let kg = {};

kg.Polygon = Polygon;
