# grunch-session

This barebones Node.js app uses [Express 4](http://expressjs.com/) to serve up a small website.

## Setup

Make sure you have [Node.js](http://nodejs.org/), then install dependencies by running:

```
$ npm install
```

## Running Locally

Run the Node server locally with:

```
$ node index.js
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

If you want to deploy the application to heroku, look up how to [![deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy).

## Grunt

TODO (see instructions.md)
